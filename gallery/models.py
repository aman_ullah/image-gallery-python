from django.db import models
from django.contrib.auth.models import User
from stdimage import StdImageField

class Gallery(models.Model):
    name = models.CharField('name of the gallery', max_length=100)
    user=models.ForeignKey(User, on_delete=models.CASCADE, related_name='galleries')

    def __str__(self):
        return self.name

class GalleryImage(models.Model):
    gallery=models.ForeignKey(Gallery, on_delete=models.CASCADE, related_name='images')
    title = models.CharField('image title', max_length=255)
    alt = models.CharField('image alt', max_length=255)
    image=StdImageField(
        variations={
            'large': (600, 400),
            'thumbnail': (100, 100, True),
            'medium': (300, 200)
        },
        delete_orphans=True,
        upload_to='uploads/'
    )

    def __str__(self):
        return self.title