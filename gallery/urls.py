from django.contrib import admin
from django.urls import path, include
from . import views
# from django.conf import settings
# from django.conf.urls.static import static

urlpatterns = [
    path('', views.index, name='galleries_index'),
    path('new', views.new, name='new_gallery'),
    path('<int:id>/edit', views.edit, name='edit_gallery'),
    path('<int:id>/delete', views.delete, name='delete_gallery'),
    path('<int:id>/images', views.images, name='gallery_images'),
    path('<int:id>/images/new', views.new_image, name='new_image'),
    path('<int:id>/images/<int:image_id>/edit', views.edit_image, name='edit_image'),
    path('<int:id>/images/<int:image_id>/delete', views.delete_image, name='delete_image'),
]
