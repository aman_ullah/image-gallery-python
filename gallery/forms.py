from django import forms
from gallery.models import Gallery, GalleryImage

class GalleryForm(forms.ModelForm):
    class Meta:
        model = Gallery
        fields = [
            'name'
        ]

class GalleryImageForm(forms.ModelForm):
    class Meta:
        model = GalleryImage
        fields = [
            'title',
            'image',
            'alt',
        ]
        