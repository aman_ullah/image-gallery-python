from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.contrib import messages
from .forms import GalleryForm, GalleryImageForm
from .models import Gallery, GalleryImage

def home(request):
    return render(request, 'gallery/home.html', {})

def index(request):
    galleries = Gallery.objects.all()
    return render(request, 'gallery/index.html', {'galleries': galleries})

def user_galleries_index(request):
    user = request.user
    if not user.is_authenticated:
        return redirect('login')
    galleries = user.galleries.all()
    print(galleries)
    return render(request, 'gallery/user_galleries.html', {'galleries': galleries})


def new(request):
    if not request.user.is_authenticated:
        return redirect('login')
    if request.method == 'POST':
        form = GalleryForm(request.POST)
        if form.is_valid():
            gallery = form.save(commit=False)
            gallery.user = request.user
            gallery.save()
            return redirect('user_galleries')
    else:
        form = GalleryForm()
    return render(request, 'gallery/new.html', {'form': form})

def edit(request, id):
    if not request.user.is_authenticated:
        return redirect('login')
    gallery = Gallery.objects.get(pk=id)
    if request.method == 'POST':
        if (not gallery) or gallery.user != request.user:
            return redirect('user_galleries')
        form = GalleryForm(request.POST, instance=gallery)
        if form.is_valid():
            gallery = form.save(commit=True)
            messages.success(request, 'Gallery updated')
            return redirect('user_galleries')
    else:
        form = GalleryForm(instance=gallery)
    
    return render(request, 'gallery/edit.html', {'form': form, 'gallery': gallery})

def delete(request, id):
    if not request.user.is_authenticated:
        return redirect('login')
    gallery = Gallery.objects.get(pk=id)

    if (gallery) and gallery.user.pk == request.user.pk:
        gallery.delete()
        messages.success(request, 'Gallery deleted.')
    return redirect('user_galleries')

def images(request, id):
    gallery = Gallery.objects.get(pk=id)
    if not gallery:
        return redirect('galleries_index')
    return render(request,'gallery/images/index.html', {'gallery': gallery})

def new_image(request, id):
    user = request.user
    if not user.is_authenticated:
        return redirect('login')
    gallery = Gallery.objects.get(pk=id)
    if (not gallery) or gallery.user.pk != user.pk:
        return redirect('galleries_index')
    if request.method == 'POST':
        form = GalleryImageForm(request.POST, request.FILES)
        if form.is_valid():
            image = form.save(commit=False)
            image.gallery = gallery
            image.save()
            return redirect('user_galleries')
    else:
        form = GalleryImageForm()
    return render(request, 'gallery/images/new.html', {'form': form, 'gallery': gallery})

def edit_image(request, id, image_id):
    user = request.user
    if not user.is_authenticated:
        return redirect('login')
    gallery = Gallery.objects.get(pk=id)
    if (not gallery) or gallery.user.pk != user.pk:
        return redirect('galleries_index')
    image = GalleryImage.objects.get(pk=image_id, gallery=gallery)
    if (not image):
        return redirect('galleries_index')
    if request.method == 'POST':
        form = GalleryImageForm(request.POST, request.FILES, instance=image)
        if form.is_valid():
            form.save(commit=True)
            messages.success(request, 'Image updated.')
            return redirect('gallery_images', id=gallery.pk)
    else:
        form = GalleryImageForm(instance=image)
    return render(request, 'gallery/images/edit.html', {'form': form, 'gallery': gallery, 'image': image})

def delete_image(request, id, image_id):
    user = request.user
    if not user.is_authenticated:
        return redirect('login')
    gallery = Gallery.objects.get(pk=id)
    if (gallery):
        image = GalleryImage.objects.get(pk=image_id, gallery=gallery)
        if image:
            image.delete()
            messages.success(request, 'Image deleted.')
    return redirect('gallery_images', id=gallery.pk)
