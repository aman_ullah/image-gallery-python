# Python version
Requires Python 3.9.6

# Package maagement

using pipenv for package management

# Install dependencies
pipenv install

# activate virtual environment

All manage.py commands should be run after starting the shell. This is done through:

    pipenv shell

# Migrate

Using sqlite for db

From shell run
    python manage.py migrate

# Reset databse
From shell run:

    python manage.py reset_db

# start server

from shell run:

    python manage.py runserver

# Admin dashboard

First create superuser in shell:

    python manage.py createsuperuser

admin area can be accessed at /admin/login/

