from django.contrib import admin
from django.urls import path
from . import views
from django.contrib.auth import views as auth_views
from gallery import views as gallery_views
# from django.conf import settings
# from django.conf.urls.static import static

urlpatterns = [
    path('register', views.register, name='register'),
    path('login', auth_views.LoginView.as_view(template_name='account/login.html'), name='login'),
    path('logout', auth_views.LogoutView.as_view(), name='logout'),
    path('galleries', gallery_views.user_galleries_index, name='user_galleries'),
]
